import 'package:test/test.dart';
import 'package:bloc_test/bloc_test.dart';

// Application packages
import 'package:puzzle_town/models/puzzle.dart';
import 'package:puzzle_town/records/puzzle.dart';
import 'package:puzzle_town/cubits/puzzle_board_cubit.dart';

void main() {
  group('PuzzleBoardCubit', () {
    late PuzzleBoardCubit puzzleBoardCubit;
    List<PuzzleRecord> puzzles = <PuzzleRecord>[
      PuzzleRecord(
        key: 'scene-1',
        name: 'Puzzle One',
        description: 'The first puzzle',
        cardCount: 2,
      ),
    ];
    PuzzleModel puzzleModel = PuzzleModel(puzzles: puzzles);

    setUp(() {
      puzzleBoardCubit = PuzzleBoardCubit(puzzleModel: puzzleModel, shuffle: false);
    });

    test('initial state is PuzzleBoardClearedState', () {
      expect(puzzleBoardCubit.state, PuzzleBoardClearedState());
    });

    blocTest(
      'emits PuzzleBoardPlayingState when play starts',
      build: () => puzzleBoardCubit,
      act: (PuzzleBoardCubit puzzleBoardCubit) =>
          puzzleBoardCubit.playPuzzle(puzzleModel.puzzles[0]),
      expect: () {
        return [
          PuzzleBoardPlayingState(
            puzzle: puzzles[0],
            puzzleCards: puzzleModel.currentPuzzleCards,
            matchedPuzzleCards: {},
            flippedPuzzleCards: {},
          )
        ];
      },
    );

    blocTest(
      'emits expected flipped PuzzleBoardPlayingState when card flipped',
      build: () => puzzleBoardCubit,
      act: (PuzzleBoardCubit puzzleBoardCubit) => puzzleBoardCubit.flipCard(0),
      expect: () {
        return [
          PuzzleBoardPlayingState(
            puzzle: puzzles[0],
            puzzleCards: puzzleModel.currentPuzzleCards,
            matchedPuzzleCards: {},
            flippedPuzzleCards: {
              0: puzzleModel.currentPuzzleCards[0],
            },
          )
        ];
      },
    );

    blocTest(
      'emits expected matched PuzzleBoardPlayingState when card flipped',
      build: () => puzzleBoardCubit,
      act: (PuzzleBoardCubit puzzleBoardCubit) => puzzleBoardCubit.flipCard(2),
      expect: () {
        return [
          PuzzleBoardPlayingState(
            puzzle: puzzles[0],
            puzzleCards: puzzleModel.currentPuzzleCards,
            matchedPuzzleCards: {
              0: puzzleModel.currentPuzzleCards[0],
              2: puzzleModel.currentPuzzleCards[2],
            },
            flippedPuzzleCards: {
              0: puzzleModel.currentPuzzleCards[0],
              2: puzzleModel.currentPuzzleCards[2],
            },
          )
        ];
      },
    );
  });
}
