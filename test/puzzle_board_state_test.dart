// Import the test package and Counter class
import 'package:test/test.dart';
import 'package:puzzle_town/cubits/puzzle_board_cubit.dart';
import 'package:puzzle_town/records/puzzle.dart';
import 'package:puzzle_town/records/puzzle_card.dart';

void main() {
  final PuzzleRecord puzzleRecord = PuzzleRecord(
    cardCount: 2,
    key: 'Key',
    name: 'Name',
    description: 'Description',
  );
  final List<PuzzleCardRecord> puzzleCardRecords = [
    PuzzleCardRecord(number: 1),
    PuzzleCardRecord(number: 2),
    PuzzleCardRecord(number: 1),
    PuzzleCardRecord(number: 2),
  ];

  final state1 = PuzzleBoardPlayingState(
    puzzle: puzzleRecord,
    puzzleCards: puzzleCardRecords,
    flippedPuzzleCards: {
      0: puzzleCardRecords[0],
      2: puzzleCardRecords[2],
    },
    matchedPuzzleCards: {},
  );

  final state2 = PuzzleBoardPlayingState(
    puzzle: puzzleRecord,
    puzzleCards: puzzleCardRecords,
    flippedPuzzleCards: {
      0: puzzleCardRecords[0],
      2: puzzleCardRecords[2],
    },
    matchedPuzzleCards: {},
  );

  final state3 = PuzzleBoardPlayingState(
    puzzle: puzzleRecord,
    puzzleCards: puzzleCardRecords,
    flippedPuzzleCards: {
      0: puzzleCardRecords[0],
      1: puzzleCardRecords[1],
    },
    matchedPuzzleCards: {},
  );

  final state4 = PuzzleBoardPlayingState(
    puzzle: puzzleRecord,
    puzzleCards: puzzleCardRecords,
    flippedPuzzleCards: {
      0: puzzleCardRecords[0],
      2: puzzleCardRecords[2],
    },
    matchedPuzzleCards: {
      0: puzzleCardRecords[0],
      2: puzzleCardRecords[2],
    },
  );

  group('PuzzleBoardState', () {
    test('matches itself', () {
      expect(
        state1,
        equals(state1),
      );
    });

    test('matches another equal state', () {
      expect(
        state1 == state2,
        true,
      );
    });

    test('does not match objects with unequal flipped card states', () {
      expect(
        state1 != state3,
        true,
      );
    });

    test('does not match objects with unequal matched card states', () {
      expect(
        state1 != state4,
        true,
      );
    });
  });
}
