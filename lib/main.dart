import 'package:flutter/material.dart';
import 'package:logging/logging.dart';
import 'package:flutter_bloc/flutter_bloc.dart';

// Application packages
import 'package:puzzle_town/data.dart';
import 'package:puzzle_town/pages/home.dart';
import 'package:puzzle_town/pages/puzzle_play.dart';
import 'package:puzzle_town/cubits/puzzle_cubit.dart';
import 'package:puzzle_town/cubits/puzzle_board_cubit.dart';
import 'package:puzzle_town/models/puzzle.dart';

final GlobalKey<NavigatorState> navigatorKey = GlobalKey<NavigatorState>();

void main() {
  Logger.root.level = Level.INFO; // defaults to Level.INFO
  Logger.root.onRecord.listen((record) {
    print('${record.level.name}: ${record.time}: ${record.loggerName} - ${record.message}');
  });

  runApp(PuzzleTownApp());
}

class PuzzleTownApp extends StatefulWidget {
  @override
  State<StatefulWidget> createState() => _PuzzleTownAppState();
}

class _PuzzleTownAppState extends State<PuzzleTownApp> {
  // Navigator pages
  List<String> _pageList = [HomePage.pageName];

  // Puzzle model
  PuzzleModel puzzleModel = PuzzleModel(puzzles: puzzles);

  // Navigator pages
  List<Page<dynamic>> _pages(BuildContext context) {
    final log = Logger('_pages');
    log.info('Building pages');
    List<Page<dynamic>> pages = [];

    _pageList.forEach((pageName) {
      switch (pageName) {
        case HomePage.pageName:
          log.info('Adding ${HomePage.pageName}');
          pages.add(HomePage());
          break;
        case PuzzlePlayPage.pageName:
          log.info('Adding ${PuzzlePlayPage.pageName}');
          pages.add(PuzzlePlayPage());
          break;
        default:
        //
      }
    });
    return pages;
  }

  // Handle specific pages being popped
  bool _onPopPage(Route<dynamic> route, dynamic result, BuildContext context) {
    final log = Logger('_onPopPage');
    log.info('Page name ${route.settings.name}');

    if (!route.didPop(result)) {
      return false;
    }

    return true;
  }

  @override
  Widget build(BuildContext context) {
    final log = Logger('build');

    log.info('Building');

    return MaterialApp(
      title: 'Puzzle Town',
      theme: ThemeData(
        primarySwatch: Colors.grey,
      ),
      home: MultiBlocProvider(
        providers: [
          BlocProvider<PuzzleCubit>(
            create: (context) => PuzzleCubit(puzzleModel: puzzleModel),
          ),
          BlocProvider<PuzzleBoardCubit>(
            create: (context) => PuzzleBoardCubit(puzzleModel: puzzleModel),
          ),
        ],
        // TODO: Move into its own stateful "navigator" widget
        child: MultiBlocListener(
          listeners: [
            BlocListener<PuzzleCubit, PuzzleState>(
              listener: (context, state) {
                log.info('Listener - PuzzleCubit changed state');
                setState(() {
                  _pageList = [
                    HomePage.pageName,
                  ];
                });
              },
            ),
            BlocListener<PuzzleBoardCubit, PuzzleBoardState>(
              listener: (context, state) {
                log.info('Listener - PuzzleBoardCubit changed state');
                if (state is PuzzleBoardPlayingState) {
                  setState(() {
                    _pageList = [
                      HomePage.pageName,
                      PuzzlePlayPage.pageName,
                    ];
                  });
                } else {
                  setState(() {
                    _pageList = [
                      HomePage.pageName,
                    ];
                  });
                }
              },
            ),
          ],
          child: Navigator(
            key: navigatorKey,
            pages: _pages(context),
            onPopPage: (route, result) => _onPopPage(route, result, context),
          ),
        ),
      ),
    );
  }
}
