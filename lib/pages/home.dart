import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:logging/logging.dart';

// Application packages
import 'package:puzzle_town/cubits/puzzle_cubit.dart';
import 'package:puzzle_town/records/puzzle.dart';
import 'package:puzzle_town/widgets/puzzle_cover.dart';

class HomePage extends Page {
  static const String pageName = 'HomePage';
  HomePage({LocalKey key = const ValueKey(HomePage.pageName), name = HomePage.pageName})
      : super(key: key, name: name);

  Route createRoute(BuildContext context) {
    return MaterialPageRoute(
      settings: this,
      builder: (BuildContext context) {
        return HomeScreen();
      },
    );
  }
}

class HomeScreen extends StatelessWidget {
  // Build puzzle grid
  List<Widget> _buildContent(
      BuildContext context, BoxConstraints constraints, List<PuzzleRecord> puzzles) {
    final log = Logger('_buildContent');

    log.info('Building');

    List<Widget> widgets = [];

    int rowItemCount = 2;
    int currentItemCount = 0;

    double containerWidth = constraints.maxWidth / rowItemCount;
    double containerHeight = constraints.maxHeight / (puzzles.length / rowItemCount);

    List<Widget> rowWidgets = [];

    for (var puzzle in puzzles) {
      if (currentItemCount == 0) {
        rowWidgets = [];
      }
      rowWidgets.add(
        GestureDetector(
          child: Container(
            width: containerWidth,
            height: containerHeight,
            color: Theme.of(context).colorScheme.surface,
            child: PuzzleCoverWidget(
              puzzle: puzzle,
            ),
          ),
        ),
      );
      currentItemCount++;
      if (currentItemCount == rowItemCount) {
        widgets.add(Row(
          children: rowWidgets,
        ));
        currentItemCount = 0;
      }
    }
    return widgets;
  }

  @override
  Widget build(BuildContext context) {
    final log = Logger('build');

    log.info('Building');

    return Scaffold(
      appBar: AppBar(
        title: Text('Puzzle Town'),
      ),
      body: Container(
        padding: EdgeInsets.all(20),
        color: Colors.blueGrey,
        child: LayoutBuilder(
          builder: (BuildContext context, BoxConstraints constraints) {
            return BlocBuilder<PuzzleCubit, PuzzleState>(
              builder: (context, state) {
                if (state is PuzzleStateInitial) {
                  log.info('Puzzle initial state ${state.puzzles.length}');
                  return Column(
                    children: _buildContent(context, constraints, state.puzzles),
                  );
                }
                log.info('Puzzle not initial state');
                return Container();
              },
            );
          },
        ),
      ),
    );
  }
}
