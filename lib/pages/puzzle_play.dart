import 'package:vector_math/vector_math.dart' as vector;
import 'package:flutter/material.dart';
import 'package:logging/logging.dart';
import 'package:flutter_bloc/flutter_bloc.dart';

// Application packages
import 'package:puzzle_town/cubits/puzzle_board_cubit.dart';
import 'package:puzzle_town/widgets/puzzle_board.dart';

class PuzzlePlayPage extends Page {
  static const String pageName = 'PuzzlePlayPage';
  PuzzlePlayPage(
      {LocalKey key = const ValueKey(PuzzlePlayPage.pageName), name = PuzzlePlayPage.pageName})
      : super(key: key, name: name);

  Route createRoute(BuildContext context) {
    return MaterialPageRoute(
      settings: this,
      builder: (BuildContext context) {
        return PuzzlePlayScreen();
      },
    );
  }
}

class PuzzlePlayScreen extends StatefulWidget {
  @override
  _PuzzlePlayScreenState createState() => _PuzzlePlayScreenState();
}

class _PuzzlePlayScreenState extends State<PuzzlePlayScreen> {
  @override
  Widget build(BuildContext context) {
    AppBar appBar = AppBar();

    return WillPopScope(
      onWillPop: () async {
        // Close puzzle when page is closed
        context.read<PuzzleBoardCubit>().closePuzzle();
        return true;
      },
      child: Scaffold(
        appBar: appBar,
        body: Container(
          padding: const EdgeInsets.all(8.0),
          child: LayoutBuilder(builder: (BuildContext context, BoxConstraints constraints) {
            final log = Logger('LayoutBuilder');

            // The width and height of the playing area needs
            // to fit a grid of 6 x 4 square cards
            double pageWidth = constraints.maxWidth;
            double pageHeight = (constraints.maxHeight - appBar.preferredSize.height);

            log.info('Play area initial width $pageWidth height $pageHeight');

            // Set initial card size based of available width
            double cardSize = constraints.maxWidth / 6;

            // Calculate required height
            pageHeight = cardSize * 4;

            log.info('Play area calculated width $pageWidth height $pageHeight');

            // When required height is greater than actual height we will
            // adjust the card size based on available height
            if (pageHeight > (constraints.maxHeight - appBar.preferredSize.height)) {
              // Set card size based on available height
              cardSize = (constraints.maxHeight - appBar.preferredSize.height) / 4;

              // Adjusted available width and height
              pageWidth = cardSize * 6;
              pageHeight = cardSize * 4;

              log.info('Play area adjusted width $pageWidth height $pageHeight');
            }

            log.info(
                'Play area final width $pageWidth height $pageHeight with card size $cardSize');

            return BlocBuilder<PuzzleBoardCubit, PuzzleBoardState>(builder: (context, state) {
              if (state is PuzzleBoardPlayingState) {
                log.info('PuzzleBoardState is playing');
                return Container(
                  color: Colors.yellow,
                  width: pageWidth,
                  height: pageHeight,
                  child: PuzzleBoardWidget(
                    puzzle: state.puzzle,
                    puzzleCards: state.puzzleCards,
                    dimensions: vector.Vector2(pageWidth, pageHeight),
                  ),
                );
              }
              log.info('PuzzleBoardState is not playing');
              return Container();
            });
          }),
        ),
      ),
    );
  }
}
