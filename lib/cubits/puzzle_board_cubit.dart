import 'package:bloc/bloc.dart';
import 'package:meta/meta.dart';
import 'package:equatable/equatable.dart';
import 'package:logging/logging.dart';

// Application packages
import 'package:puzzle_town/records/puzzle.dart';
import 'package:puzzle_town/records/puzzle_card.dart';
import 'package:puzzle_town/models/puzzle.dart';

part 'puzzle_board_state.dart';

// Logger
final glog = Logger('PuzzleBoardCubit');
Logger getLogger(String name) {
  return Logger(glog.fullName + ' - $name');
}

class PuzzleBoardCubit extends Cubit<PuzzleBoardState> {
  PuzzleModel puzzleModel;
  bool shuffle;
  PuzzleBoardCubit({required this.puzzleModel, this.shuffle = true})
      : super(
          PuzzleBoardClearedState(),
        );

  void playPuzzle(PuzzleRecord puzzle) {
    puzzleModel.startPlayingPuzzle(puzzle, shuffle: shuffle);
    emit(PuzzleBoardPlayingState(
      puzzle: puzzleModel.currentPuzzle!,
      puzzleCards: puzzleModel.currentPuzzleCards,
      flippedPuzzleCards: puzzleModel.currentFlippedPuzzleCards,
      matchedPuzzleCards: puzzleModel.currentMatchedPuzzleCards,
    ));
  }

  PuzzleRecord? currentPuzzle() {
    return puzzleModel.currentPuzzle;
  }

  void closePuzzle() {
    puzzleModel.stopPlayingPuzzle();
    emit(PuzzleBoardClearedState());
  }

  void flipCard(int idx) {
    final log = getLogger('flipCard');

    bool flipped = puzzleModel.flipCard(idx);
    log.info('Emitting state - card idx >$idx< flipped >$flipped<');

    emit(
      PuzzleBoardMatchingState(
        puzzle: puzzleModel.currentPuzzle!,
        puzzleCards: puzzleModel.currentPuzzleCards,
        flippedPuzzleCards: puzzleModel.currentFlippedPuzzleCards,
        matchedPuzzleCards: puzzleModel.currentMatchedPuzzleCards,
      ),
    );

    bool matched = puzzleModel.matchFlipped();
    log.info('Emitting state - card idx >$idx< matched >$matched<');

    emit(
      PuzzleBoardPlayingState(
        puzzle: puzzleModel.currentPuzzle!,
        puzzleCards: puzzleModel.currentPuzzleCards,
        flippedPuzzleCards: puzzleModel.currentFlippedPuzzleCards,
        matchedPuzzleCards: puzzleModel.currentMatchedPuzzleCards,
      ),
    );
  }
}
