part of 'puzzle_cubit.dart';

@immutable
abstract class PuzzleState extends Equatable {}

class PuzzleStateInitial extends PuzzleState {
  final List<PuzzleRecord> puzzles;
  PuzzleStateInitial(this.puzzles);

  @override
  List<Object> get props => [puzzles];
}
