import 'package:bloc/bloc.dart';
import 'package:meta/meta.dart';
import 'package:equatable/equatable.dart';

// Application packages
import 'package:puzzle_town/records/puzzle.dart';
import 'package:puzzle_town/models/puzzle.dart';

part 'puzzle_state.dart';

class PuzzleCubit extends Cubit<PuzzleState> {
  PuzzleModel puzzleModel;
  PuzzleCubit({required this.puzzleModel})
      : super(
          PuzzleStateInitial(
            puzzleModel.puzzles,
          ),
        );
}
