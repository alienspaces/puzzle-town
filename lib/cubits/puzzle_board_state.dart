part of 'puzzle_board_cubit.dart';

@immutable
abstract class PuzzleBoardState extends Equatable {}

// PuzzleBoardClearedState has no puzzle and no cards
class PuzzleBoardClearedState extends PuzzleBoardState {
  PuzzleBoardClearedState();

  @override
  List<Object> get props => [];
}

// PuzzleBoardPlayingState has a puzzle and shuffled cards
// that may each be flipped or not flipped, matched or not
// matched. The PuzzleBoard
class PuzzleBoardMatchingState extends PuzzleBoardState {
  final PuzzleRecord puzzle;
  final List<PuzzleCardRecord> puzzleCards;
  final Map<int, PuzzleCardRecord> flippedPuzzleCards;
  final Map<int, PuzzleCardRecord> matchedPuzzleCards;

  PuzzleBoardMatchingState({
    required this.puzzle,
    required this.puzzleCards,
    required this.flippedPuzzleCards,
    required this.matchedPuzzleCards,
  });

  onChange(Change<PuzzleBoardMatchingState> change) {
    final log = Logger('PuzzleBoardMatchingState - onChange');
    log.info('Change $change');
  }

  @override
  List<Object> get props => [
        puzzle,
        puzzleCards,
        flippedPuzzleCards,
        matchedPuzzleCards,
      ];
}

// PuzzleBoardPlayingState has a puzzle and shuffled cards
// that may each be flipped or not flipped, matched or not
// matched. The PuzzleBoard
class PuzzleBoardPlayingState extends PuzzleBoardState {
  final PuzzleRecord puzzle;
  final List<PuzzleCardRecord> puzzleCards;
  final Map<int, PuzzleCardRecord> flippedPuzzleCards;
  final Map<int, PuzzleCardRecord> matchedPuzzleCards;

  PuzzleBoardPlayingState({
    required this.puzzle,
    required this.puzzleCards,
    required this.flippedPuzzleCards,
    required this.matchedPuzzleCards,
  });

  onChange(Change<PuzzleBoardPlayingState> change) {
    final log = Logger('PuzzleBoardPlayingState - onChange');
    log.info('Change $change');
  }

  @override
  List<Object> get props => [
        puzzle,
        puzzleCards,
        flippedPuzzleCards,
        matchedPuzzleCards,
      ];

  // @override
  // bool get stringify => true;
}
