import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:logging/logging.dart';
import 'package:puzzle_town/cubits/puzzle_board_cubit.dart';

// Application packages
import 'package:puzzle_town/records/puzzle.dart';

typedef ClickCallback = bool Function(int idx);

class PuzzleCoverWidget extends StatefulWidget {
  final PuzzleRecord puzzle;

  PuzzleCoverWidget({Key? key, required this.puzzle}) : super(key: key);

  @override
  _PuzzleCoverWidgetState createState() => _PuzzleCoverWidgetState();
}

class _PuzzleCoverWidgetState extends State<PuzzleCoverWidget> {
  // getPuzzleCoverAsset returns the asset path for the puzzle cover
  String getPuzzleCoverAsset() {
    return 'assets/scenes/${widget.puzzle.key}/scene.png';
  }

  // getPuzzleCoverImage returns the asset image for the puzzle cover
  Image getPuzzleCoverImage() {
    return Image.asset(
      getPuzzleCoverAsset(),
      fit: BoxFit.fill,
    );
  }

  void _onClick(BuildContext context) {
    final log = Logger('_onClick');
    log.info('Click ${widget.puzzle.name}');
    context.read<PuzzleBoardCubit>().playPuzzle(widget.puzzle);
  }

  @override
  Widget build(BuildContext context) {
    return LayoutBuilder(builder: (BuildContext context, BoxConstraints constraints) {
      return GestureDetector(
        onTap: () => _onClick(context),
        child: Stack(
          children: <Widget>[
            // Puzzle name
            Container(
              child: Text('${widget.puzzle.name}'),
            ),
            // Puzzle image
            Container(
              color: Colors.yellow,
              height: constraints.maxHeight,
              width: constraints.maxWidth,
              child: getPuzzleCoverImage(),
            ),
          ],
        ),
      );
    });
  }
}
