import 'package:flip_card/flip_card_controller.dart';
import 'package:flip_card/flip_card.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:flutter/material.dart';
import 'package:logging/logging.dart';

// Application packages
import 'package:puzzle_town/cubits/puzzle_board_cubit.dart';
import 'package:puzzle_town/records/puzzle_card.dart';
import 'package:puzzle_town/records/puzzle.dart';

// Logger
final glog = Logger('PuzzleCardWidget');
Logger getLogger(String name) {
  return Logger(glog.fullName + ' - $name');
}

class PuzzleCardWidget extends StatefulWidget {
  final int idx;
  final PuzzleRecord puzzle;
  final PuzzleCardRecord puzzleCard;

  PuzzleCardWidget({
    Key? key,
    required this.idx,
    required this.puzzle,
    required this.puzzleCard,
  }) : super(key: key);

  @override
  _PuzzleCardWidgetState createState() => _PuzzleCardWidgetState();
}

class _PuzzleCardWidgetState extends State<PuzzleCardWidget> {
  FlipCardController? controller;

  @override
  void initState() {
    controller = FlipCardController();
    super.initState();
  }

  // getPuzzleCardAsset returns the asset path for the puzzle card
  String getPuzzleCardAsset() {
    return 'assets/scenes/${widget.puzzle.key}/cards/card-${widget.puzzleCard.number}.png';
  }

  // getPuzzleCardImage returns the asset image for the puzzle card
  Image getPuzzleCardImage() {
    return Image.asset(
      getPuzzleCardAsset(),
      fit: BoxFit.fill,
    );
  }

  // getCardBackImage returns the asset image for the puzzle card back
  Image getCardBackImage() {
    return Image.asset(
      'assets/unflipped.png',
      fit: BoxFit.fill,
    );
  }

  void _onClick(BuildContext context) {
    final log = getLogger('_onClick - idx ${widget.idx}');
    // bool canFlip = context.read<PuzzleBoardCubit>().flipCard(widget.idx);
    // log.info(
    //     'Card idx ${widget.idx} number ${widget.puzzleCard.number} flipped ${widget.puzzleCard.flipped}');
    // if (canFlip) {
    //   log.info('Card idx ${this.widget.idx} flipping');
    //   controller?.toggleCard();
    // }
    context.read<PuzzleBoardCubit>().flipCard(widget.idx);
    log.info('Number ${widget.puzzleCard.number} Flipped ${widget.puzzleCard.flipped}');
    // if (canFlip) {
    //   log.info('Card idx ${this.widget.idx} flipping');
    //   controller?.toggleCard();
    // }
  }

  void _onChange(BuildContext context, PuzzleBoardState state) {
    final log = getLogger('_onChange - idx ${widget.idx}');
    log.info('Number ${widget.puzzleCard.number} Flipped ${widget.puzzleCard.flipped}');
    if (state is! PuzzleBoardPlayingState) {
      log.info('PuzzleBoardState is not playing');
      return;
    }

    log.info('PuzzleBoardState is playing, checking flipped');
    PuzzleCardRecord puzzleCard = state.puzzleCards[widget.idx];
    if (puzzleCard.flipped != widget.puzzleCard.flipped) {
      log.info('Flipping card');
      controller?.toggleCard();
      setState(() {
        widget.puzzleCard.flipped = puzzleCard.flipped;
      });
    }
  }

  @override
  Widget build(BuildContext context) {
    final log = getLogger('build');

    log.info(
      'Card idx ${widget.idx} number ${widget.puzzleCard.number} flipped ${widget.puzzleCard.flipped}',
    );

    if (widget.puzzleCard.flipped) {
      log.info('Flipping card');
      controller?.toggleCard();
    }

    return BlocListener<PuzzleBoardCubit, PuzzleBoardState>(
      listenWhen: (previousState, state) {
        log.info('Listen when called');
        // return true/false to determine whether or not
        // to call listener with state
        return true;
      },
      listener: (context, state) => _onChange,
      child: Container(
        child: GestureDetector(
          onTap: () => _onClick(context),
          child: FlipCard(
            flipOnTouch: false,
            front: Container(
              color: Colors.orange,
              child: getCardBackImage(),
            ),
            back: Container(
              color: Colors.amber,
              child: getPuzzleCardImage(),
            ),
            controller: controller,
          ),
        ),
      ),
    );
  }
}
