// import 'dart:math';
import 'package:vector_math/vector_math.dart' as vector;
import 'package:flutter/material.dart';
import 'package:logging/logging.dart';
import 'package:flutter_bloc/flutter_bloc.dart';

// Application packages
import 'package:puzzle_town/cubits/puzzle_board_cubit.dart';
import 'package:puzzle_town/records/puzzle.dart';
import 'package:puzzle_town/records/puzzle_card.dart';
import 'package:puzzle_town/widgets/puzzle_card.dart';

// Logger
final glog = Logger('PuzzleBoardWidget');
Logger getLogger(String name) {
  return Logger(glog.fullName + ' - $name');
}

class PuzzleBoardWidget extends StatefulWidget {
  final PuzzleRecord puzzle;
  final List<PuzzleCardRecord> puzzleCards;
  final vector.Vector2 dimensions;

  PuzzleBoardWidget({
    required this.puzzle,
    required this.puzzleCards,
    required this.dimensions,
  });

  @override
  _PuzzleBoardWidgetState createState() => _PuzzleBoardWidgetState();
}

class _PuzzleBoardWidgetState extends State<PuzzleBoardWidget> {
  // getPuzzleCoverAsset returns the asset path for the puzzle cover
  String getPuzzleCoverAsset() {
    return 'assets/scenes/${widget.puzzle.key}/scene.png';
  }

  // getPuzzleCoverImage returns the asset image the puzzle cover
  Image getPuzzleCoverImage() {
    return Image.asset(
      getPuzzleCoverAsset(),
      fit: BoxFit.fill,
    );
  }

  List<Widget> _buildContent(BuildContext context) {
    final log = getLogger('_buildContent');

    // Make this list of widgets part of this widgets state so we can
    // modify them only when underlying Bloc state changes
    List<Widget> widgets = [];

    // Adjust card width and height based on the size of the available space
    double cardWidth = (widget.dimensions.x - (6 * 7)) / 6;
    double cardHeight = (widget.dimensions.y - (4 * 7)) / 4;

    log.info('Cards length ${widget.puzzleCards.length} width $cardWidth height $cardHeight');

    var idx = 0;
    List<Widget> rowWidgets = [];
    widget.puzzleCards.forEach((puzzleCard) {
      rowWidgets.add(
        Container(
          width: cardWidth,
          height: cardHeight,
          margin: EdgeInsets.all(3),
          child: PuzzleCardWidget(
            idx: idx,
            puzzle: widget.puzzle,
            puzzleCard: puzzleCard,
          ),
        ),
      );
      idx++;
      if (idx % 6 == 0) {
        widgets.add(
          Row(
            children: rowWidgets,
          ),
        );
        rowWidgets = [];
      }
    });

    return widgets;
  }

  @override
  Widget build(BuildContext context) {
    final log = getLogger('build');
    return BlocConsumer<PuzzleBoardCubit, PuzzleBoardState>(
      buildWhen: (previous, current) {
        // return true/false to determine whether or not
        // to rebuild the widget with state
        return true;
      },
      builder: (context, state) {
        log.info('Building');
        if (state is PuzzleBoardPlayingState) {
          log.info('PuzzleBoardState is playing');
          return Stack(
            children: <Widget>[
              Container(
                width: widget.dimensions.x,
                height: widget.dimensions.y,
                child: getPuzzleCoverImage(),
              ),
              Container(
                child: Column(
                  children: _buildContent(context),
                ),
              ),
            ],
          );
        }
        log.info('PuzzleBoardState is not playing');
        return Container();
      },
      listener: (context, state) {
        //
      },
    );
  }
}
