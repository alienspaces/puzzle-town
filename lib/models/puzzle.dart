import 'dart:math';
import 'package:logging/logging.dart';

// Application packages
import 'package:puzzle_town/records/puzzle.dart';
import 'package:puzzle_town/records/puzzle_card.dart';

enum Difficulty { easy, moderate, hard }

// PuzzleModel
class PuzzleModel {
  // Available puzzles
  final List<PuzzleRecord> puzzles;

  // Current puzzle
  PuzzleRecord? _currentPuzzle;

  // Current puzzle cards
  List<PuzzleCardRecord> _currentPuzzleCards = [];

  // Current flipped puzzle cards
  Map<int, PuzzleCardRecord> _currentFlippedPuzzleCards = {};

  // Current matched puzzle cards
  Map<int, PuzzleCardRecord> _currentMatchedPuzzleCards = {};

  // Difficulty level
  Difficulty difficultyLevel = Difficulty.easy;

  // Constructor
  PuzzleModel({required this.puzzles});

  // currentPuzzle is the puzzle that is currently being played
  PuzzleRecord? get currentPuzzle {
    return _currentPuzzle;
  }

  // currentPuzzleCards are the shuffled cards of the current puzzle
  List<PuzzleCardRecord> get currentPuzzleCards {
    return _currentPuzzleCards;
  }

  // currentFlippedPuzzleCards are the shuffled cards of the current puzzle
  Map<int, PuzzleCardRecord> get currentFlippedPuzzleCards {
    return _currentFlippedPuzzleCards;
  }

  // currentMatchedPuzzleCards are the shuffled cards of the current puzzle
  Map<int, PuzzleCardRecord> get currentMatchedPuzzleCards {
    return _currentMatchedPuzzleCards;
  }

// Return number of card decks in puzzle
  int _deckCount() {
    switch (difficultyLevel) {
      case Difficulty.easy:
        return 2;
      case Difficulty.moderate:
        return 3;
      case Difficulty.hard:
        return 4;
    }
  }

  // startPlayingPuzzle
  void startPlayingPuzzle(PuzzleRecord puzzle, {bool shuffle = true}) {
    var random = new Random();

    // Set current puzzle
    _currentPuzzle = puzzle;

    // Reset current flipped card list
    _currentFlippedPuzzleCards = {};

    // Reset current matched card list
    _currentMatchedPuzzleCards = {};

    // Build and shuffle puzzle card deck
    for (var deckCount = 0; deckCount < _deckCount(); deckCount++) {
      for (var cardCount = 0; cardCount < puzzle.cardCount; cardCount++) {
        PuzzleCardRecord cardRecord = PuzzleCardRecord(
          number: cardCount,
        );
        _currentPuzzleCards.add(cardRecord);
      }
    }
    if (shuffle) {
      _currentPuzzleCards.shuffle(random);
    }
  }

  // stopPlayingPuzzle
  void stopPlayingPuzzle() {
    // Reset current puzzle
    _currentPuzzle = null;

    // Reset current puzzle cards
    _currentPuzzleCards = [];

    // Reset current flipped card list
    _currentFlippedPuzzleCards = {};

    // Reset current matched card list
    _currentMatchedPuzzleCards = {};
  }

  // flipCard
  bool flipCard(int idx) {
    final log = Logger('PuzzleModel - flipCard');

    if (_currentPuzzle == null) {
      return false;
    }
    if (_currentPuzzleCards.length < idx) {
      return false;
    }

    // Unflip
    if (_currentPuzzleCards[idx].flipped) {
      log.info('Unflipping, removing from flipped list');
      _currentFlippedPuzzleCards.remove(idx);
      _currentPuzzleCards[idx].flipped = false;
      log.info(
        'Card idx >$idx< number >${_currentPuzzleCards[idx].number}< flipped >${_currentPuzzleCards[idx].flipped}<',
      );
      return true;
    }

    // Only two cards are allowed to be flipped at the same time
    if (_currentFlippedPuzzleCards.length == 2) {
      log.info('Two cards flipped, cannot flip');
      return false;
    }

    log.info('Flipping, adding to flipped list');

    _currentFlippedPuzzleCards[idx] = _currentPuzzleCards[idx];
    _currentPuzzleCards[idx].flipped = true;

    log.info(
      'Card idx >$idx< number >${_currentPuzzleCards[idx].number}< flipped >${_currentPuzzleCards[idx].flipped}<',
    );

    return true;
  }

  // Checks if flipped cards match
  bool matchFlipped() {
    final log = Logger('PuzzleModel - matchFlipped');

    log.info('Current flipped card list length >${_currentFlippedPuzzleCards.length}<');

    // For a successful match the number of flipped cards
    // must equal the number of decks in play
    if (_currentFlippedPuzzleCards.length != _deckCount()) {
      return false;
    }

    log.info('Testing matched');

    // Matched card list of indexes
    List<int> matchedCardList = [];

    int? cardNumber;

    _currentFlippedPuzzleCards.forEach((int idx, PuzzleCardRecord card) {
      log.info('Testing idx >$idx< number >${card.number}<');
      if (cardNumber == null) {
        cardNumber = card.number;
        matchedCardList.add(idx);
        return;
      }
      if (card.number == cardNumber) {
        matchedCardList.add(idx);
      }
    });

    // When the number of flipped cards that match is equal to the puzzle deck
    // count we can then update the matched state of the current puzzle cards.
    if (matchedCardList.length == _deckCount()) {
      log.info('Match! Card number $cardNumber');
      matchedCardList.forEach((idx) {
        // Update puzzle card list state
        _currentPuzzleCards[idx].matched = true;
        // Update matched puzzle card list
        _currentMatchedPuzzleCards[idx] = _currentPuzzleCards[idx];
      });
      return true;
    }

    return false;
  }
}
