// Application Packages
import 'package:puzzle_town/records/puzzle.dart';

// Available puzzles
List<PuzzleRecord> puzzles = [
  PuzzleRecord(
    key: 'scene-1',
    name: 'Puzzle One',
    description: 'The first puzzle',
    cardCount: 12,
  ),
  PuzzleRecord(
    key: 'scene-2',
    name: 'Puzzle Two',
    description: 'The second puzzle',
    cardCount: 12,
  ),
  PuzzleRecord(
    key: 'scene-3',
    name: 'Puzzle Three',
    description: 'The third puzzle',
    cardCount: 12,
  ),
  PuzzleRecord(
    key: 'scene-4',
    name: 'Puzzle Four',
    description: 'The fourth puzzle',
    cardCount: 12,
  ),
];
