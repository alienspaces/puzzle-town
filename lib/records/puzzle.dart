// PuzzleRecord
class PuzzleRecord {
  // Identifier used to identify assets
  String key;
  // Display name and description
  String name;
  String description;
  // Number of cards to be found
  int cardCount;
  int completedCount = 0;
  int abandonedCount = 0;
  bool playing = false;

  PuzzleRecord({
    required this.key,
    required this.name,
    required this.description,
    required this.cardCount,
  });
}
