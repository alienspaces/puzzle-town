class PuzzleCardRecord {
  final int number;
  bool flipped = false;
  bool matched = false;

  PuzzleCardRecord({required this.number});
}
